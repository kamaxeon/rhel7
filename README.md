# Backport for RHEL7

This repo contains the scripts and CI jobs for building newer versions of
different pieces of software on **RHEL7**, to be used by the RHEL7 builder
container images at **CKI**.

## Software backported

Currently the following software are being compiled on RHEL6:

* **Python 3.9.13**

## How is it used?

The software compiled here will be stored as artifacts to be used at
build time for the [builder-rhel7],[builder-rhel7.3], [builder-rhel7.4],
[builder-rhel7.6], [builder-rhel7.7] templates at the [CKI containers repository].

Using the feature [needs] from Gitlab CI, an artifact from this project can be
used from another project.

> NOTE: Using artifacts between projects is a premium feature. CKI uses it
> because is part of the [Gitlab Open Source program].

Here is an example of how to use it from another project:

```yaml
builder-rhel7:
  extends: [.publish_local, .internal_regs]
  needs:
    - project: cki-project/backports/rhel6
      job: build-python
      ref: main
      artifacts: true
```

[builder-rhel7]: https://gitlab.com/cki-project/containers/-/blob/main/builds/builder-rhel7.in
[builder-rhel7.3]: https://gitlab.com/cki-project/containers/-/blob/main/builds/builder-rhel7.3.in
[builder-rhel7.4]: https://gitlab.com/cki-project/containers/-/blob/main/builds/builder-rhel7.4.in
[builder-rhel7.6]: https://gitlab.com/cki-project/containers/-/blob/main/builds/builder-rhel7.6.in
[builder-rhel7.7]: https://gitlab.com/cki-project/containers/-/blob/main/builds/builder-rhel7.7.in
[CKI containers repository]: https://gitlab.com/cki-project/containers/
[needs]: https://docs.gitlab.com/ee/ci/yaml/README.html#cross-project-artifact-downloads-with-needs
[Gitlab Open Source program]: https://about.gitlab.com/solutions/open-source/
